$$\boldsymbol{a\,A \longrightarrow c\,C}$$

$$\;\;\;\;\;\;\;\;\;\; \frac{ d\,\alpha}{d\,t}=k(T)\cdot f(\alpha) \;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\, (2)$$

$$\;\;\;\;\;\ \alpha = \frac{A_0-A}{A_0} = \frac{a}{c}\cdot \frac{C}{A_{0}} \;\;\;\;\;\;\;\;\;\;\;\;\; (3)$$

$$f(\alpha)=a^m \cdot(1-\alpha)^n = \alpha \cdot (1-\alpha) \;\;\;\;(4)$$

$$\;\;\;\;\;\;\;\;\;\;\; k(T) = B\cdot e^{-{E_a}/{R\,T}}\;\;\;\;\;\;\;\;\;\;\;\;\;\;\; (5)$$

$$\;\;\;\; P = k(T)\cdot \alpha \cdot (1-\alpha)\cdot A_0\cdot {\scriptstyle \Delta} H\;\;\;\;\; (6) $$

