                      benzoyl-tetra-Ac-salicin     Library Search Report

  Data Path : D:\DATA\20160219\
  Data File : R-5.D                                               
  Acq On    : 19 Feb 2016  15:14
  Operator  : kassanova
  Sample    :  
  Misc      :  
  ALS Vial  : 1   Sample Multiplier: 1

  Search Libraries:   C:\Database\NIST08.L              Minimum Quality:   0

  Unknown Spectrum:   Apex minus start of peak
  Integration Events: ChemStation Integrator - autoint1.e

Pk#     RT  Area%          Library/ID                 Ref#     CAS#   Qual
_____________________________________________________________________________
  1  13.314 55.69 C:\Database\NIST08.L
                 4-Nitrophenyl trifluoromethanesulf 114532 017763-80-3 55
                 onate
                 3-Nitrophenyl trifluoromethanesulf 114533 032578-25-9 45
                 onate
                 Propanoic acid, 2-chloro-            5174 000598-78-7 23
 
  2  14.912 44.31 C:\Database\NIST08.L
                 Benzene, 1-iodo-4-nitro-            97160 000636-98-6 94
                 Benzene, 1-iodo-3-nitro-            97162 000645-00-1 93
                 Benzene, 1-iodo-2-nitro-            97161 000609-73-4 74
 

UNIVERSAL_ARENES.M Fri Feb 19 15:44:28 2016   
