
  Data Path : D:\Data\20160219\
                       benzoyl-tetra-Ac-salicin     Area Percent Report
  Data File : R-5.D                                               
  Acq On    : 19 Feb 2016  15:14
  Operator  : kassanova
  Sample    :  
  Misc      :  
  ALS Vial  : 1   Sample Multiplier: 1

  Integration Parameters: autoint1.e
  Integrator: ChemStation

  Method    : C:\msdchem\1\METHODS\pyridines.M
  Title     :  

  Signal     : TIC: R-5.D\data.ms

 peak  R.T. first  max last  PK   peak      corr.   corr.    % of
   #   min   scan scan scan  TY  height     area    % max.   total
 ---  ----- ----- ---- ---- ---  -------   -------  ------  -------
  1  13.316  1358 1363 1370 BB    292045   4269229 100.00%  55.597%
  2  14.913  1569 1576 1584 BB    205879   3409668  79.87%  44.403%
 
 
                        Sum of corrected areas:     7678896

pyridines.M Fri Feb 19 15:53:47 2016   
