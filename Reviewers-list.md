
1. **François-Xavier Felpin**, Université de Nantes; UFR des Sciences et des Techniques, CNRS UMR 6230, CEISAM, 2 rue de la Houssinière, 44322 Nantes Cedex 3, France. E-mail: fx.felpin@univ-nantes.fr
2. **Markus R. Heinrich.**, Department of Chemistry and Pharmacy, Pharmaceutical Chemistry, Friedrich-Alexander-Universitat Erlangen-Nürnberg, Nikolaus-Fiebiger-Straße 10, 91058 Erlangen, Germany, E-mail address: markus.heinrich@fau.de
3. **Prof. Dr. Stefan Bräse**, Dipl.-Chem. M. Schroen University of Karlsruhe (TH), Fritz-Haber-Weg 6 76131 Karlsruhe (Germany) Fax: (+49)721-608-8581, E-mail: braese@ioc.uka.de
4. **Sergey V. Bondarchuk**, Department of Chemistry and Nanomaterials Science, Bogdan Khmelnitsky Cherkasy National University, blvd, Shevchenko 81, 18031 Cherkasy, Ukraine. Email: bondchem@cdu.edu.ua
5. **Min Sheng**, Reactive Chemical Capability / Dow Chemical Co., Midland, MI, 48667, USA

